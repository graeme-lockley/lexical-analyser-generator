package za.co.no9.lag.re.text

import io.kotlintest.shouldBe
import io.kotlintest.specs.ExpectSpec
import za.co.no9.lag.data.result.Left
import za.co.no9.lag.data.result.Right
import za.co.no9.lag.re.ast.*


class ParserTest : ExpectSpec({
    context("concatenation") {
        expect("hello") {
            parser("hello") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Literal('h'),
                                    Literal('e'),
                                    Literal('l'),
                                    Literal('l'),
                                    Literal('o'))))
        }
    }


    context("alternative") {
        expect("aa|bb|cc") {
            parser("aa|bb|cc") shouldBe
                    Right<String, AST>(
                            Alternative(listOf(
                                    Concatenation(listOf(Literal('a'), Literal('a'))),
                                    Concatenation(listOf(Literal('b'), Literal('b'))),
                                    Concatenation(listOf(Literal('c'), Literal('c'))))))
        }
    }


    context("zero or one") {
        expect("a?") {
            parser("a?") shouldBe
                    Right<String, AST>(
                            ZeroOrOne(
                                    Literal('a')))
        }

        expect("a??") {
            parser("a??") shouldBe
                    Right<String, AST>(
                            ZeroOrOne(
                                    ZeroOrOne(
                                            Literal('a'))))
        }
    }


    context("zero or more") {
        expect("a*") {
            parser("a*") shouldBe
                    Right<String, AST>(
                            ZeroOrMore(
                                    Literal('a')))
        }

        expect("a**") {
            parser("a**") shouldBe
                    Right<String, AST>(
                            ZeroOrMore(
                                    ZeroOrMore(
                                            Literal('a'))))
        }
    }


    context("one or more") {
        expect("a+") {
            parser("a+") shouldBe
                    Right<String, AST>(
                            OneOrMore(
                                    Literal('a')))
        }

        expect("a++") {
            parser("a++") shouldBe
                    Right<String, AST>(
                            OneOrMore(
                                    OneOrMore(
                                            Literal('a'))))
        }
    }


    context("group") {
        expect("(a)") {
            parser("(a)") shouldBe
                    Right<String, AST>(
                            Literal('a'))
        }

        expect("(a|b)c") {
            parser("(a|b)c") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Alternative(listOf(
                                            Literal('a'),
                                            Literal('b'))),
                                    Literal('c'))))
        }

        expect("(a") {
            parser("(a") shouldBe
                    Left<String, AST>(
                            "2: Expected ')'")
        }
    }


    context("dot") {
        expect(".") {
            parser(".") shouldBe
                    Right<String, AST>(
                            Dot)
        }
    }


    context("char") {
        expect("*") {
            parser("*") shouldBe
                    Left<String, AST>(
                            "0: '*' requires an operand")
        }

        expect("a\\\\b") {
            parser("a\\\\b") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Literal('a'),
                                    Literal('\\'),
                                    Literal('b'))))
        }

        expect("a\\|b") {
            parser("a\\|b") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Literal('a'),
                                    Literal('|'),
                                    Literal('b'))))
        }

        expect("a\\?b") {
            parser("a\\?b") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Literal('a'),
                                    Literal('?'),
                                    Literal('b'))))
        }

        expect("a\\*b") {
            parser("a\\*b") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Literal('a'),
                                    Literal('*'),
                                    Literal('b'))))
        }

        expect("a\\+b") {
            parser("a\\+b") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Literal('a'),
                                    Literal('+'),
                                    Literal('b'))))
        }

        expect("a\\(b") {
            parser("a\\(b") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Literal('a'),
                                    Literal('('),
                                    Literal('b'))))
        }

        expect("a\\)b") {
            parser("a\\)b") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Literal('a'),
                                    Literal(')'),
                                    Literal('b'))))
        }

        expect("a\\nb") {
            parser("a\\nb") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Literal('a'),
                                    Literal('\n'),
                                    Literal('b'))))
        }

        expect("a\\tb") {
            parser("a\\tb") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Literal('a'),
                                    Literal('\t'),
                                    Literal('b'))))
        }

        expect("a\\ b") {
            parser("a\\ b") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Literal('a'),
                                    Literal(' '),
                                    Literal('b'))))
        }

        expect("a\\Xb") {
            parser("a\\Xb") shouldBe
                    Left<String, AST>("2: Illegal escape of 'X'")
        }
    }


    context("combined") {
        expect("(H|h)ello (W|w)orld") {
            parser("(H|h)ello (W|w)orld") shouldBe
                    Right<String, AST>(
                            Concatenation(listOf(
                                    Alternative(listOf(
                                            Literal('H'),
                                            Literal('h')
                                    )),
                                    Literal('e'),
                                    Literal('l'),
                                    Literal('l'),
                                    Literal('o'),
                                    Literal(' '),
                                    Alternative(listOf(
                                            Literal('W'),
                                            Literal('w')
                                    )),
                                    Literal('o'),
                                    Literal('r'),
                                    Literal('l'),
                                    Literal('d')
                            )))
        }
    }
})