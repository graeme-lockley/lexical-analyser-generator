package za.co.no9.lag.dfa

import io.kotlintest.data.forall
import io.kotlintest.properties.Gen
import io.kotlintest.properties.assertAll
import io.kotlintest.shouldBe
import io.kotlintest.specs.ExpectSpec
import io.kotlintest.tables.row
import za.co.no9.lag.data.result.Right
import za.co.no9.lag.dfa.UnitDFA
import za.co.no9.lag.nfa.UnitNFA
import za.co.no9.lag.nfa.fromRE
import za.co.no9.lag.re.text.parser


class EpsilonClosureTest : ExpectSpec({
    context("-> 0") {
        expect("0") {
            calculateStartState(UnitNFA(0, finalState(1),
                    listOf(
                            listOf(Pair(setOf('a'), 1)),
                            emptyList()
                    )
            )) shouldBe setOf(0)
        }
    }

    context("-> 0 -> 1") {
        expect("0, 1") {
            calculateStartState(UnitNFA(0, finalState(2),
                    listOf(
                            listOf(Pair(setOf('a'), 2), Pair(setOf(), 1)),
                            listOf(Pair(setOf('a'), 2)),
                            emptyList()
                    )
            )) shouldBe setOf(0, 1)
        }
    }

    context("-> 0 -> 1 -> 2") {
        expect("0, 1, 2") {
            calculateStartState(UnitNFA(0, finalState(3),
                    listOf(
                            listOf(Pair(setOf('a'), 3), Pair(setOf(), 1)),
                            listOf(Pair(setOf('a'), 3), Pair(setOf(), 2)),
                            listOf(Pair(setOf('a'), 3)),
                            emptyList()
                    )
            )) shouldBe setOf(0, 1, 2)
        }
    }

    context("-> 0; 0 -> 0") {
        expect("0") {
            calculateStartState(UnitNFA(0, finalState(1),
                    listOf(
                            listOf(Pair(setOf('a'), 1), Pair(setOf(), 0)),
                            emptyList()
                    )
            )) shouldBe setOf(0)
        }
    }

    context("-> 0 -> 2; 1 -> 2; 2 -> 1") {
        expect("0") {
            calculateStartState(UnitNFA(0, finalState(1),
                    listOf(
                            listOf(Pair(setOf('a'), 3), Pair(setOf(), 2)),
                            listOf(Pair(setOf('a'), 3), Pair(setOf(), 2)),
                            listOf(Pair(setOf('a'), 3), Pair(setOf(), 1)),
                            emptyList()
                    )
            )) shouldBe setOf(0, 1, 2)
        }
    }
})


fun finalState(s: Int): Map<NodeRef, Unit> =
        mapOf(Pair(s, Unit))

fun calculateStartState(nfa: UnitNFA): Set<NodeRef> {
    val epsilonTransitiveClosure =
            transitiveClosure(nfa.epsilonTransitions())

    return epsilonTransitiveClosure[nfa.startNode] + nfa.startNode
}


class NFAtoDFATest : ExpectSpec({
    context("(a|b).") {
        val builtIn =
                Regex("[ab].")

        val regex =
                parse("(a|b).")

        expect("random scenarios") {
            assertAll(Gen.string(3)) { t: String ->
                match(regex, t) shouldBe builtIn.matches(t)
            }
        }
    }

    context("(H|h)ello (W|w)orld") {
        val builtIn =
                Regex("[Hh]ello [Ww]orld")

        val regex =
                parse("(H|h)ello (W|w)orld")

        expect("random scenarios") {
            assertAll(Gen.string(11)) { t: String ->
                match(regex, t) shouldBe builtIn.matches(t)
            }
        }

        expect("all the different valid scenarios") {
            forall(
                    row("Hello world"),
                    row("Hello World"),
                    row("hello World"),
                    row("hello world")
            ) {
                match(regex, it) shouldBe true
            }
        }
    }
})


fun parse(text: String): UnitDFA =
        (parser(text)
                .map { fromRE(it, Unit) }
                .map { fromNFA(it) } as Right).right
