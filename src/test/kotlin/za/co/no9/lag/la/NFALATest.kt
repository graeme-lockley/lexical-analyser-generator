package za.co.no9.lag.la

import io.kotlintest.shouldBe
import io.kotlintest.specs.ExpectSpec
import za.co.no9.lag.data.result.Right
import za.co.no9.lag.dfa.DFA
import za.co.no9.lag.dfa.fromNFA
import za.co.no9.lag.nfa.NFA
import za.co.no9.lag.re.ast.AST
import za.co.no9.lag.re.text.parser


class NFALATest : ExpectSpec({
    context("Translation scenarios") {
        expect("(a|b)+ (a|b)+\\.(a|b)+") {
            val definition =
                    Definition(
                            { x, y -> x < y },
                            listOf(
                                    Pair(parse("(a|b)+"), 0),
                                    Pair(parse("(a|b)+X(a|b)+"), 1)
                            )
                    )

            fromDefinition(definition) shouldBe
                    NFA(0, mapOf(Pair(1, 0), Pair(5, 1)),
                            listOf(
                                    listOf(Pair(setOf(), 4), Pair(setOf(), 12)),              // 0
                                    listOf(Pair(setOf(), 4)),                                 // 1
                                    listOf(Pair(setOf('a'), 1)),                              // 2
                                    listOf(Pair(setOf('b'), 1)),                              // 3
                                    listOf(Pair(setOf(), 2), Pair(setOf(), 3)),               // 4
                                    listOf(Pair(setOf(), 8)),                                 // 5
                                    listOf(Pair(setOf('a'), 5)),                              // 6
                                    listOf(Pair(setOf('b'), 5)),                              // 7
                                    listOf(Pair(setOf(), 6), Pair(setOf(), 7)),               // 8
                                    listOf(Pair(setOf('X'), 8), Pair(setOf(), 12)),           // 9
                                    listOf(Pair(setOf('a'), 9)),                              // 10
                                    listOf(Pair(setOf('b'), 9)),                              // 11
                                    listOf(Pair(setOf(), 10), Pair(setOf(), 11))              // 12
                            )
                    )
        }
    }
})


class DFALATest : ExpectSpec({
    context("Translation scenarios") {
        expect("(a|b)+ (a|b)+\\.(a|b)+") {
            val definition =
                    Definition(
                            { x, y -> x < y },
                            listOf(
                                    Pair(parse("(a|b)+"), 0),
                                    Pair(parse("(a|b)+X(a|b)+"), 1)
                            )
                    )

            fromNFA(fromDefinition(definition), definition.isLessThan) shouldBe
                    DFA(0, mapOf(Pair(1, 0), Pair(3, 1)),
                            listOf(
                                    listOf(Pair(setOf('a', 'b'), 1)),                       // 0
                                    listOf(Pair(setOf('X'), 2), Pair(setOf('a', 'b'), 1)),  // 1
                                    listOf(Pair(setOf('a', 'b'), 3)),                       // 2
                                    listOf(Pair(setOf('a', 'b'), 3))                        // 3
                            )
                    )
        }

        expect("(a|b)+ (a|b)+ (a|b)+\\.(a|b)+ - the second (a|b)+ disappears as it's token value is greater than the first") {
            val definition =
                    Definition(
                            { x, y -> x < y },
                            listOf(
                                    Pair(parse("(a|b)+"), 0),
                                    Pair(parse("(a|b)+"), 2),
                                    Pair(parse("(a|b)+X(a|b)+"), 1)
                            )
                    )

            fromNFA(fromDefinition(definition), definition.isLessThan) shouldBe
                    DFA(0, mapOf(Pair(1, 0), Pair(3, 1)),
                            listOf(
                                    listOf(Pair(setOf('a', 'b'), 1)),                       // 0
                                    listOf(Pair(setOf('X'), 2), Pair(setOf('a', 'b'), 1)),  // 1
                                    listOf(Pair(setOf('a', 'b'), 3)),                       // 2
                                    listOf(Pair(setOf('a', 'b'), 3))                        // 3
                            )
                    )
        }

        expect("(a|b)+ (a|b)+ (a|b)+\\.(a|b)+ - the first (a|b)+ disappears as it's token value is greater than the second") {
            val definition =
                    Definition(
                            { x, y -> x > y },
                            listOf(
                                    Pair(parse("(a|b)+"), 0),
                                    Pair(parse("(a|b)+"), 2),
                                    Pair(parse("(a|b)+X(a|b)+"), 1)
                            )
                    )

            fromNFA(fromDefinition(definition), definition.isLessThan) shouldBe
                    DFA(0, mapOf(Pair(1, 2), Pair(3, 1)),
                            listOf(
                                    listOf(Pair(setOf('a', 'b'), 1)),                       // 0
                                    listOf(Pair(setOf('X'), 2), Pair(setOf('a', 'b'), 1)),  // 1
                                    listOf(Pair(setOf('a', 'b'), 3)),                       // 2
                                    listOf(Pair(setOf('a', 'b'), 3))                        // 3
                            )
                    )
        }
    }
})


fun parse(text: String): AST =
        (parser(text) as Right).right