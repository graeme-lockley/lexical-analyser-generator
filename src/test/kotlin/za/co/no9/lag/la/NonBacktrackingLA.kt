package za.co.no9.lag.la

import io.kotlintest.shouldBe
import io.kotlintest.specs.ExpectSpec
import za.co.no9.lag.dfa.DFA
import za.co.no9.lag.dfa.fromNFA
import kotlin.test.assertFailsWith

class NonBacktrackingLA : ExpectSpec({
    context("basic lower ID and whitespace LA definition") {
        val dfa =
                Definition(
                        { x, y -> x < y },
                        listOf(
                                Pair(parse("(a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z)+"), 1),
                                Pair(parse(" +"), 2)
                        )).toDFA()

        expect(" hello  world   ") {
            val tokens =
                    tokenizer(dfa, " hello  world   ").toList()

            tokens.size shouldBe 5
            tokens[0] shouldBe Token(2, " ")
            tokens[1] shouldBe Token(1, "hello")
            tokens[2] shouldBe Token(2, "  ")
            tokens[3] shouldBe Token(1, "world")
            tokens[4] shouldBe Token(2, "   ")
        }

        expect("hello 123") {
            val exception =
                    assertFailsWith<LexicalException> { tokenizer(dfa, "hello 123").toList() }

            exception.character shouldBe '1'
            exception.possibleCharacters shouldBe ('a'..'z').toSet().union(setOf(' '))
            exception.stateIndex shouldBe 0
            exception.index shouldBe 6
        }
    }
})


fun Definition<Int>.toDFA(): DFA<Int> =
        fromNFA(fromDefinition(this), this.isLessThan)