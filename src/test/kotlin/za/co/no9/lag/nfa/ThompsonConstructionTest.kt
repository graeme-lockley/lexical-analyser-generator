package za.co.no9.lag.nfa

import io.kotlintest.shouldBe
import io.kotlintest.specs.ExpectSpec
import za.co.no9.lag.data.result.Right
import za.co.no9.lag.re.text.parser


fun finalState(nodeRef: NodeRef): Map<NodeRef, Unit> =
        mapOf(Pair(nodeRef, Unit))

class ThompsonConstructionTest : ExpectSpec({
    context("Dot") {
        expect("'.'") {
            parser(".").map { fromRE(it) } shouldBe
                    Right<String, UnitNFA>(
                            UnitNFA(1, finalState(0),
                                    listOf(
                                            emptyList(),
                                            listOf(Pair((0..255).map { it.toChar() }.toSet(), 0))
                                    )
                            ))
        }
    }

    context("Literal") {
        expect("'a'") {
            parser("a").map { fromRE(it) } shouldBe
                    Right<String, UnitNFA>(UnitNFA(1, finalState(0),
                            listOf(
                                    emptyList(),
                                    listOf(Pair(setOf('a'), 0))
                            )
                    ))
        }
    }

    context("Concatenation") {
        expect("'abc'") {
            parser("abc").map { fromRE(it) } shouldBe
                    Right<String, UnitNFA>(UnitNFA(3, finalState(0),
                            listOf(
                                    emptyList(),
                                    listOf(Pair(setOf('c'), 0)),
                                    listOf(Pair(setOf('b'), 1)),
                                    listOf(Pair(setOf('a'), 2))
                            )
                    ))
        }
    }

    context("Alternative") {
        expect("'a|b|c'") {
            parser("a|b|c").map { fromRE(it) } shouldBe
                    Right<String, UnitNFA>(UnitNFA(4, finalState(0),
                            listOf(
                                    emptyList(),
                                    listOf(Pair(setOf('a'), 0)),
                                    listOf(Pair(setOf('b'), 0)),
                                    listOf(Pair(setOf('c'), 0)),
                                    listOf(Pair(setOf(), 1), Pair(setOf(), 2), Pair(setOf(), 3))
                            )
                    ))
        }
    }

    context("ZeroOrMore") {
        expect("'a*'") {
            parser("a*").map { fromRE(it) } shouldBe
                    Right<String, UnitNFA>(UnitNFA(1, finalState(0),
                            listOf(
                                    listOf(Pair(setOf(), 1)),
                                    listOf(Pair(setOf('a'), 0), Pair(setOf(), 0))
                            )
                    ))
        }
    }

    context("OneOrMore") {
        expect("'a+'") {
            parser("a+").map { fromRE(it) } shouldBe
                    Right<String, UnitNFA>(UnitNFA(1, finalState(0),
                            listOf(
                                    listOf(Pair(setOf(), 1)),
                                    listOf(Pair(setOf('a'), 0))
                            )
                    ))
        }
    }

    context("ZeroOrOne") {
        expect("'a?'") {
            parser("a?").map { fromRE(it) } shouldBe
                    Right<String, UnitNFA>(UnitNFA(1, finalState(0),
                            listOf(
                                    emptyList(),
                                    listOf(Pair(setOf('a'), 0), Pair(setOf(), 0))
                            )
                    ))
        }
    }
})
