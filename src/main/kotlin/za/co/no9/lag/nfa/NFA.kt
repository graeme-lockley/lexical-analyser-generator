package za.co.no9.lag.nfa

import za.co.no9.lag.re.ast.*


typealias NodeRef =
        Int


data class NFA<T>(
        val startNode: NodeRef,
        val endNodes: Map<NodeRef, T>,
        val nodes: List<List<Pair<Set<Char>, NodeRef>>>) {
    fun epsilonTransitions() =
            nodes.map { it.filter { p -> p.first.isEmpty() }.map { p -> p.second }.toSet() }
}


typealias UnitNFA =
        NFA<Unit>


class NFABuilder<T> {
    private var startState: Node? =
            null

    private val endState: MutableMap<NodeRef, T> =
            mutableMapOf()

    private val nodes =
            mutableListOf<Node>()

    fun addStartState(state: Node): NFABuilder<T> {
        startState = state
        return this
    }

    fun addFinalState(state: Node, tokenID: T): NFABuilder<T> {
        endState[state.id] = tokenID
        return this
    }

    fun addNode(): Node {
        val result =
                Node(nodes.size, mutableListOf())

        nodes.add(result)

        return result
    }

    fun build(): NFA<T> =
            NFA(startState!!.id, endState, nodes.map { node -> node.transitions.map { Pair(it.alphabet, it.next.id) } })
}


fun <T> fromRE(re: AST, result: T): NFA<T> {
    val builder =
            NFABuilder<T>()

    val finalState =
            builder.addNode()

    return builder
            .addFinalState(finalState, result)
            .addStartState(thompsonConstruction({ builder.addNode() }, re, finalState))
            .build()
}

fun fromRE(re: AST): UnitNFA =
        fromRE(re, Unit)


data class Node(val id: NodeRef, val transitions: MutableList<Transition> = mutableListOf()) {
    fun addTransition(alphabet: Set<Char>, next: Node): Node {
        transitions.add(Transition(alphabet, next))

        return this
    }
}


data class Transition(val alphabet: Set<Char>, val next: Node)


private val setOfAllChars =
        (0..255).map { it.toChar() }.toSet()


fun thompsonConstruction(nodeFactory: () -> Node, re: AST, finalState: Node): Node =
        when (re) {
            is Dot ->
                nodeFactory().addTransition(setOfAllChars, finalState)

            is Literal ->
                nodeFactory().addTransition(setOf(re.c), finalState)

            is Concatenation ->
                re.res.foldRight(finalState, { ast, tail -> thompsonConstruction(nodeFactory, ast, tail) })

            is Alternative ->
                re.res.map { thompsonConstruction(nodeFactory, it, finalState) }.fold(nodeFactory(), { a, b -> a.addTransition(emptySet(), b) })

            is ZeroOrMore -> {
                val result =
                        thompsonConstruction(nodeFactory, re.re, finalState)

                result.addTransition(emptySet(), finalState)
                finalState.addTransition(emptySet(), result)

                result
            }

            is OneOrMore -> {
                val result =
                        thompsonConstruction(nodeFactory, re.re, finalState)

                finalState.addTransition(emptySet(), result)

                result
            }

            is ZeroOrOne ->
                thompsonConstruction(nodeFactory, re.re, finalState)
                        .addTransition(emptySet(), finalState)
        }