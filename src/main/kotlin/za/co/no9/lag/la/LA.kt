package za.co.no9.lag.la

import za.co.no9.lag.dfa.DFA
import za.co.no9.lag.nfa.NFA
import za.co.no9.lag.nfa.NFABuilder
import za.co.no9.lag.nfa.thompsonConstruction
import za.co.no9.lag.re.ast.AST


typealias TokenID =
        Int

data class Definition<T>(
        val isLessThan: (T, T) -> Boolean,
        val patterns: List<Pair<AST, T>>)


fun <T> fromDefinition(definition: Definition<T>): NFA<T> {
    val builder =
            NFABuilder<T>()

    val startState =
            builder.addNode()

    builder.addStartState(startState)

    definition.patterns.forEach { pattern ->
        val finalState =
                builder.addNode()

        builder.addFinalState(finalState, pattern.second)
        startState.addTransition(setOf(), thompsonConstruction({ builder.addNode() }, pattern.first, finalState))
    }

    return builder.build()
}


data class Token(val id: Int, val lexeme: String)

class LexicalException(val index: Int, val stateIndex: Int, val character: Char, val possibleCharacters: Set<Char>) : java.lang.IllegalArgumentException("Lexical Error: $stateIndex") {
    override fun toString(): String =
            "LexicalException: index=$index, stateIndex: $stateIndex, character: '$character' (${character.toInt()}), possibleCharacters: $possibleCharacters"
}

private class TokenizerIterator(val dfa: DFA<TokenID>, val text: String) : AbstractIterator<Token>() {
    private val textLength =
            text.length

    private var index =
            0

    override fun computeNext() {
        if (index >= textLength) {
            done()
        } else {
            var currentState =
                    dfa.startNode

            val startIndex =
                    index

            while (index < textLength) {
                val c =
                        text[index]

                val nextState =
                        dfa.nodes[currentState].find { it.first.contains(c) }

                if (nextState == null) {
                    val result =
                            dfa.endNodes[currentState]

                    if (result == null) {
                        throw LexicalException(index, currentState, c, dfa.nodes[currentState].fold(emptySet(), { a, b -> a.union(b.first) }))
                    } else {
                        setNext(Token(result, text.substring(startIndex, index)))
                    }
                    return
                } else {
                    index += 1
                    currentState = nextState.second
                }
            }

            val result =
                    dfa.endNodes[currentState]

            if (result == null) {
                done()
            } else {
                setNext(Token(result, text.substring(startIndex)))
            }
        }
    }

}


fun tokenizer(dfa: DFA<TokenID>, text: String): Sequence<Token> =
        Sequence { TokenizerIterator(dfa, text) }