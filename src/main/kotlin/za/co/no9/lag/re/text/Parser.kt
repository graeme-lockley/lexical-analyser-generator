package za.co.no9.lag.re.text

import za.co.no9.lag.data.result.Left
import za.co.no9.lag.data.result.Result
import za.co.no9.lag.data.result.Right
import za.co.no9.lag.re.ast.*


/*
<RE> ::=
    <simple-RE> { "|" <simple-RE> }

<simple-RE> ::=
    <basic-RE> { <basic-RE> }

<basic-RE> ::=
    <elementary-RE> { "*" | "+" | "?" }

<elementary-RE> ::=
    <group> | <any> | <char> | <set>

<group> ::=
    "(" <RE> ")"

<any> ::=
    "."

<char> ::=
    any non metacharacter | "\" metacharacter

<set> ::=
    <positive-set> | <negative-set>

<positive-set> ::=
    "[" <set-items> "]"

<negative-set> ::=
    "[^" <set-items> "]"

<set-items>  ::=
    <set-item> | <set-item> <set-items>

<set-items>  ::=
    <range> | <char>

<range> ::=
     <char> "-" <char>
*/


private const val EOS =
        (-1).toChar()


private val markupMetaCharacters =
        setOf('|', ')', '(', '+', '*', '?', EOS)

private val simpleREMetaCharacters =
        setOf('|', ')', EOS)


private class REParserException(offset: Int, msg: String) : Exception("$offset: $msg")


fun parser(input: String): Result<String, AST> {
    class Parser(val parserState: ParserState) {

        fun parseElementaryRE(): AST {
            when (parserState.current) {
                '(' -> {
                    parserState.skip()

                    val result =
                            parseRE()

                    if (parserState.current == ')') {
                        parserState.skip()
                    } else {
                        throw REParserException(parserState.offset, "Expected ')'")
                    }

                    return result
                }

                '.' -> {
                    parserState.skip()
                    return Dot
                }

                '\\' -> {
                    parserState.skip()

                    fun skipReturn(c: Char): AST {
                        parserState.skip()
                        return Literal(c)
                    }

                    return when (parserState.current) {
                        '\\' -> skipReturn('\\')
                        '|' -> skipReturn('|')
                        '?' -> skipReturn('?')
                        '*' -> skipReturn('*')
                        '+' -> skipReturn('+')
                        '(' -> skipReturn('(')
                        ')' -> skipReturn(')')
                        ' ' -> skipReturn(' ')
                        'n' -> skipReturn('\n')
                        't' -> skipReturn('\t')
                        else ->
                            throw REParserException(parserState.offset, "Illegal escape of '${parserState.current}'")
                    }
                }

                else -> {
                    if (parserState.current elementOf markupMetaCharacters) {
                        throw REParserException(parserState.offset, "'${parserState.current}' requires an operand")
                    } else {
                        val result =
                                Literal(parserState.current)

                        parserState.skip()

                        return result
                    }
                }
            }
        }

        fun parseBasicRE(): AST {
            var result =
                    parseElementaryRE()

            while (true) {
                if (parserState.current == '*') {
                    result = ZeroOrMore(result)
                    parserState.skip()
                } else if (parserState.current == '+') {
                    result = OneOrMore(result)
                    parserState.skip()
                } else if (parserState.current == '?') {
                    result = ZeroOrOne(result)
                    parserState.skip()
                } else {
                    break
                }
            }

            return result
        }

        fun parseSimpleRE(): AST {
            val result =
                    parseBasicRE()

            return if (parserState.current elementOf simpleREMetaCharacters) {
                result
            } else {
                val items =
                        mutableListOf(result)

                while (parserState.current notElementOf simpleREMetaCharacters) {
                    items.add(parseBasicRE())
                }

                Concatenation(items)
            }
        }

        fun parseRE(): AST {
            val result =
                    parseSimpleRE()

            return when {
                parserState.current == '|' -> {
                    val items =
                            mutableListOf(result)

                    while (parserState.current == '|') {
                        parserState.skip()
                        items.add(parseSimpleRE())
                    }

                    Alternative(items)
                }

                else ->
                    result
            }
        }
    }

    return try {
        Right(Parser(ParserState(input)).parseRE())
    } catch (e: Exception) {
        Left(e.message!!)
    }
}


private class ParserState(private val input: String) {
    private var index: Int =
            0

    private val inputLength =
            input.length

    val current: Char
        get() =
            if (index >= inputLength) EOS else input[index]

    val offset: Int
        get() =
            index

    fun skip() {
        index += 1
    }
}


private infix fun Char.elementOf(s: Set<Char>) =
        s.contains(this)

private infix fun Char.notElementOf(s: Set<Char>) =
        !s.contains(this)