package za.co.no9.lag.re.ast

sealed class AST

object Dot : AST()

data class Literal(val c: Char) : AST()

data class Concatenation(val res: List<AST>) : AST()

data class Alternative(val res: List<AST>) : AST()

data class ZeroOrMore(val re: AST) : AST()

data class OneOrMore(val re: AST) : AST()

data class ZeroOrOne(val re: AST) : AST()
