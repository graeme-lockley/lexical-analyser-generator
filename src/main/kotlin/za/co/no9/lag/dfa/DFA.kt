package za.co.no9.lag.dfa

import za.co.no9.lag.nfa.NFA


typealias NodeRef =
        Int

data class DFA<T>(
        val startNode: NodeRef,
        val endNodes: Map<NodeRef, T>,
        val nodes: List<List<Pair<Set<Char>, NodeRef>>>)


fun <T> fromNFA(nfa: NFA<T>, isLessThan: (T, T) -> Boolean = { _, _ -> true }): DFA<T> {
    val epsilonClosure =
            transitiveClosure(nfa.epsilonTransitions())

    val toVisit =
            mutableSetOf<Set<NodeRef>>()

    val nodes =
            mutableListOf<List<Pair<Set<Char>, NodeRef>>>()

    val mapping =
            mutableMapOf<Set<NodeRef>, NodeRef>()

    fun resolveState(stateRef: Set<NodeRef>): NodeRef {
        val m =
                mapping[stateRef]

        return if (m == null) {
            val result =
                    nodes.size

            mapping[stateRef] = result
            nodes.add(mutableListOf())
            toVisit.add(stateRef)

            result
        } else {
            m
        }
    }

    fun finalTokenID(nodes: Set<NodeRef>): T {
        val tokens =
                nodes.map { nfa.endNodes.getValue(it) }

        return tokens.drop(1).fold(tokens[0], { a, b ->
            if (isLessThan(a, b))
                a
            else
                b
        })
    }


    val startState =
            resolveState(epsilonClosure[nfa.startNode] + nfa.startNode)

    while (toVisit.isNotEmpty()) {
        val nodeRef =
                toVisit.first()

        toVisit.remove(nodeRef)

        val reachableNodes =
                nodeRef.map { epsilonClosure[it] }.fold(nodeRef, { a, b -> a.union(b) })

        val traversals =
                reachableNodes.flatMap { nfa.nodes[it] }

        val alphabet =
                traversals.fold(emptySet<Char>(), { a, b -> a.union(b.first) })

        val reverseMapping =
                mutableMapOf<NodeRef, MutableSet<Char>>()

        alphabet.forEach {
            val targetNodes =
                    resolveState(traversals.filter { s -> s.first.contains(it) }.map { it.second }.toSet())

            val rm =
                    reverseMapping[targetNodes]

            if (rm == null) {
                reverseMapping[targetNodes] = mutableSetOf(it)
            } else {
                rm.add(it)
            }
        }

        nodes[resolveState(nodeRef)] =
                reverseMapping.toList().map { a -> Pair(a.second, a.first) }
    }

    val endNodesKeys =
            nfa.endNodes.keys

    val finalStateMapping =
            mapping.entries
                    .map { a -> a.key }
                    .filter { x -> x.intersect(endNodesKeys).isNotEmpty() }
                    .map { Pair(resolveState(it), finalTokenID(it.intersect(endNodesKeys))) }
                    .fold(emptyMap<NodeRef, T>(), { a, b ->
                        a + b
                    })

    return DFA(startState, finalStateMapping, nodes)
}


typealias UnitDFA =
        DFA<Unit>


fun <T> match(dfa: DFA<T>, text: String): Boolean {
    var currentState =
            dfa.startNode

    var idx =
            0

    val textLength =
            text.length

    while (idx < textLength) {
        val c =
                text[idx]

        val nextState =
                dfa.nodes[currentState].find { it.first.contains(c) }

        if (nextState == null) {
            return false
        } else {
            idx += 1
            currentState = nextState.second
        }
    }

    return dfa.endNodes.contains(currentState)
}


fun transitiveClosure(matrix: List<Set<Int>>): List<Set<Int>> {
    val result =
            matrix.toMutableList()

    val dimension =
            result.size

    for (count in 0..dimension) {
        for (iteration in 0 until dimension) {
            result[iteration] = result[iteration].map { result[it] }.fold(result[iteration], { acc, i -> acc.union(i) })
        }
    }

    return result
}



