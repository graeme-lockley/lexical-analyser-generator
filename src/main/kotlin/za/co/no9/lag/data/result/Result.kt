package za.co.no9.lag.data.result

interface Result<L, R> {
    infix fun <U> map(f: (R) -> U): Result<L, U>

    infix fun <U> mapLeft(f: (L) -> U): Result<U, R>

    infix fun <U> andThen(f: (R) -> Result<L, U>): Result<L, U>
}


data class Left<L, R>(val left: L) : Result<L, R> {
    override fun <U> map(f: (R) -> U): Result<L, U> =
            Left(left)

    override fun <U> mapLeft(f: (L) -> U): Result<U, R> =
            Left(f(left))

    override fun <U> andThen(f: (R) -> Result<L, U>): Result<L, U> =
            Left(left)
}


data class Right<L, R>(val right: R) : Result<L, R> {
    override fun <U> map(f: (R) -> U): Result<L, U> =
            Right(f(right))

    override fun <U> mapLeft(f: (L) -> U): Result<U, R> =
            Right(right)

    override fun <U> andThen(f: (R) -> Result<L, U>): Result<L, U> =
            f(right)
}
